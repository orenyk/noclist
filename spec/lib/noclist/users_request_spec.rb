RSpec.describe Noclist::UsersRequest do
  describe ".call" do
    let(:auth_token) { "5AA3B3B5-2C0C-72F8-A9D3-4AE537982D5D" }
    let(:checksum) do
      "e6fcca264d42c6d26124ca7c8307b65b29c8f4afffe954db6a29d87384fc747f"
    end
    let(:expected_users) { %w(userid1 userid2 userid3) }
    let(:response_body) { expected_users.join("\n") }
    let(:client) { instance_spy(Noclist::Client) }
    let(:response) do
      double("StringIO",
             status: ["200", "OK"],
             string: response_body
            )
    end

    before do
      allow(client).to receive(:get)
        .with(path: "/users", headers: { "X-Request-Checksum" => checksum })
        .and_return(response)
    end

    it "returns the list of user IDs as an array" do
      user_ids = described_class.call(client: client, auth_token: auth_token)
      expect(user_ids).to match_array(expected_users)
    end
  end
end
