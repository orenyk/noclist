RSpec.describe Noclist::Client do
  describe "#get" do
    let(:host) { "foo.example.com" }
    let(:path) { "/bar" }

    context "without headers" do
      before do
        stub_request(:get, "#{host}#{path}").to_return(
          status: 200,
          body: "Success"
        )
      end

      it "returns the response object with the status code" do
        client = described_class.new(api_base: "http://#{host}")
        response = client.get(path: path)
        expect(response.status.first).to eq("200")
      end

      it "returns the response object with the body" do
        client = described_class.new(api_base: "http://#{host}")
        response = client.get(path: path)
        expect(response.string).to eq("Success")
      end
    end

    context "with headers" do
      let(:headers) { { "Header-Foo" => "Bar" } }

      before do
        stub_request(:get, "#{host}#{path}").with(headers: headers)
          .to_return(status: 200, body: "Success")
      end

      it "sends the correct request" do
        client = described_class.new(api_base: "http://#{host}")
        response = client.get(path: path, headers: headers)
        expect(response.string).to eq("Success")
      end
    end

    context "with up to two failures" do
      before do
        stub_request(:get, "#{host}#{path}").to_return(
          { status: 500 },
          { status: 500 },
          { status: 200, body: "Success" }
        )
      end

      it "succeeds" do
        client = described_class.new(api_base: "http://#{host}")
        response = client.get(path: path)
        expect(response.status.first).to eq("200")
      end
    end

    context "with three failures" do
      before do
        stub_request(:get, "#{host}#{path}").to_return(
          { status: 500 },
          { status: 500 },
          { status: 500 },
          { status: 200, body: "Success" }
        )
      end

      it "raises the appropriate exception" do
        client = described_class.new(api_base: "http://#{host}")
        expect { client.get(path: path) }.to raise_error(Noclist::Error)
      end
    end

    context "with timeout" do
      before do
        stub_request(:get, "#{host}#{path}")
          .to_timeout.then
          .to_return(status: 200, body: "Success")
      end

      it "tries again" do
        client = described_class.new(api_base: "http://#{host}")
        response = client.get(path: path)
        expect(response.status.first).to eq("200")
      end
    end

    context "with mixed failures and timeouts" do
      before do
        stub_request(:get, "#{host}#{path}")
          .to_timeout.then
          .to_return(
            { status: 500 },
            { status: 200, body: "Success" }
          )
      end

      it "succeeds" do
        client = described_class.new(api_base: "http://#{host}")
        response = client.get(path: path)
        expect(response.status.first).to eq("200")
      end
    end

    context "with redirects" do
      before do
        stub_request(:get, "#{host}#{path}").to_return(
          # should raise a Webmock exception if followed
          { status: 307, headers: { 'Location' => 'http://www.example.com' } },
          { status: 301, headers: { 'Location' => 'http://www.example.com' } },
          { status: 200, body: "Success" }
        )
      end

      it "ignores them" do
        client = described_class.new(api_base: "http://#{host}")
        response = client.get(path: path)
        expect(response.string).to eq("Success")
      end
    end
  end
end
