RSpec.describe Noclist::AuthRequest do
  describe ".call" do
    let(:expected_token) { "6B4DB228-D4B0-706B-1147-D24408B48602" }
    let(:client) { instance_spy(Noclist::Client) }
    let(:response) do
      double("StringIO",
             meta: { "badsec-authentication-token" => expected_token },
             status: ["200", "OK"],
             string: "E4800421C6AAB1E19367B93B15265EDA"
            )
    end

    before do
      allow(client).to receive(:get).with(path: "/auth").and_return(response)
    end

    it "returns the auth token from the header" do
      token = described_class.call(client: client)
      expect(token).to eq(expected_token)
    end
  end
end
