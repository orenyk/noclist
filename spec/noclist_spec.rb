RSpec.describe Noclist do
  it "has a version number" do
    expect(Noclist::VERSION).not_to be nil
  end

  describe ".run" do
    let(:host) { "foo.example.com" }

    context "with working API" do
      let(:auth_token) { "60E91616-F00F-9A7C-F27B-1C1B9AD49593" }
      let(:user_ids) { %w(userid1 userid2 userid3) }

      before { stub_api(host, auth_token, user_ids) }

      it "prints the user IDs to stdout" do
        fake_stdout = StringIO.new
        described_class.run(stdout: fake_stdout, api_base: "http://#{host}")
        expect(JSON.parse(fake_stdout.string)).to match_array(user_ids)
      end

      def stub_api(host, auth_token, user_ids)
        stub_auth(host, auth_token)
        stub_users(host, auth_token, user_ids)
      end

      def stub_auth(host, auth_token)
        stub_request(:get, "#{host}/auth").to_return(
          status: 200,
          body: "auth_body",
          headers: { "Badsec-Authentication-Token" => auth_token }
        )
      end

      def stub_users(host, auth_token, user_ids)
        checksum = Digest::SHA256.hexdigest("#{auth_token}/users")
        stub_request(:get, "#{host}/users")
          .with(headers: { "X-Request-Checksum" => checksum })
          .to_return(
            status: 200,
            body: user_ids.join("\n")
          )
      end
    end

    context "with failing API" do
      before do
        stub_request(:any, "#{host}/auth").to_return(status: 500)
        stub_request(:any, "#{host}/users").to_return(status: 500)
      end

      it "prints to stderr" do
        fake_stderr = StringIO.new
        described_class.run(stderr: fake_stderr, api_base: "http://#{host}")
        expect(fake_stderr.string).to eq("Error: Too many failures\n")
      end

      it "exits with status code 1" do
        expect { described_class.run(api_base: "http://#{host}") }.to \
          raise_error(SystemExit)
      end
    end
  end
end
