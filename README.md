# Noclist

This is a Ruby utility to retrieve the list of VIP users from the Bureau of
Adversarial Dossiers and Securely Encrypted Code (BADSEC) API. It handles
authentication with the server and prints the list of user IDs to stdout.

## Installation

    $ git clone https://gitlab.com/orenyk/noclist.git
    $ cd noclist
    $ bin/setup
    $ bundle exec rake install

## Usage

    $ noclist <API base URL, e.g. http://localhost:8888>

## Development

You should have a working Ruby 2.7 environment.

After checking out the repo, run `bin/setup` to install dependencies. Then, run
`bundle exec rake spec` to run the tests. You can also run `bin/console` for an
interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.

## License

The gem is available as open source under the terms of the
[MIT License](https://opensource.org/licenses/MIT).
