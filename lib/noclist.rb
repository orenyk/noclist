require "noclist/client"
require "noclist/auth_request"
require "noclist/users_request"
require "noclist/version"
require "json"

module Noclist
  class Error < StandardError; end

  # Run the script
  #
  # @param api_base [String] the base URL for the API
  # @param stdout [IO] the IO object to use for stdout, defaults to $stdout
  # @param stderr [IO] the IO object to use for stderr, defaults to $stderr
  def self.run(api_base:, stdout: $stdout, stderr: $stderr)
    # api_base = get_api_base(options)
    client = Client.new(api_base: api_base)
    auth_token = AuthRequest.call(client: client)
    user_ids = UsersRequest.call(client: client, auth_token: auth_token)
    stdout.puts user_ids.to_json
  rescue Error => e
    stderr.puts "Error: #{e.message}"
    exit(1)
  end
end
