require 'open-uri'

module Noclist
  # Client object for all requests to BADSEC API
  class Client
    # Instatiation method
    #
    # @param api_base [String] the base URL for the API
    def initialize(api_base:)
      @api_base = api_base
    end

    # Send a GET request to a specific endpoint, returns a string extended by
    # OpenURI::Meta. If we needed multiple HTTP verbs we'd likely use Net::HTTP
    # or a more functional HTTP client (like HTTParty)
    #
    # @param path [String] the path to request
    # @param headers [Hash<String => String>] the headers to pass with the
    #   request
    # @return the response object
    def get(path:, headers: {})
      uri = URI.parse("#{api_base}#{path}")
      send_request(uri, headers, NUM_TRIES)
    end

    private

    NUM_TRIES = 3

    attr_reader :api_base

    def send_request(uri, headers, num_tries)
      raise Error.new("Too many failures") unless num_tries.positive?

      # We need to disable redirects under the assumption that non-200 responses
      # are failures.
      uri.open(**headers.merge({ redirect: false }))
    rescue OpenURI::HTTPError, Net::OpenTimeout
      send_request(uri, headers, num_tries - 1)
    end
  end
end
