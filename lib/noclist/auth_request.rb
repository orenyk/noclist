module Noclist
  # Object that retrieves auth tokens from the server
  class AuthRequest
    # Retrieve the auth token from the server
    #
    # @param client [Client] the client to use for the request
    # @return [String] the auth token
    def self.call(client:)
      response = client.get(path: '/auth')
      # OpenURI dowcases header keys
      response.meta[TOKEN_HEADER.downcase]
    end

    private

    # storing the actual header key here in case the HTTP implementation needs
    # to change
    TOKEN_HEADER = "Badsec-Authentication-Token"
  end
end
