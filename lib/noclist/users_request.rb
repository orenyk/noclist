require 'digest'

module Noclist
  # Object that retrieves the list of user IDs from the server
  class UsersRequest
    # Request the list of user IDs from the server
    #
    # @param client [Client] the client to use for the request
    # @param auth_token [String] the auth token to use for the checksum
    def self.call(client:, auth_token:)
      checksum = generate_checksum(auth_token)
      response = client.get(path: API_PATH,
                            headers: { CHECKSUM_HEADER => checksum })
      response.string.split("\n")
    end

    private

    API_PATH = "/users"
    CHECKSUM_HEADER = "X-Request-Checksum"

    def self.generate_checksum(auth_token)
      Digest::SHA256.hexdigest("#{auth_token}#{API_PATH}")
    end
  end
end
